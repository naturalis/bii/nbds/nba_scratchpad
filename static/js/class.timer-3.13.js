class Timer extends Base {

	selector_timer = '#timer'
	selector_timer_clipboard = '#timerClipboard'

	QUERY_EXECUTION = 'timerEventQueryExecution'

	runningTimers = []
	prevService = null
	timerCumulative = 0
	executionCount = 0
	timerVisible = false
	lastQueryName = ""

    constructor(config,server)
    {
        super(config);
        this.setServer(server);
    }

	reset( timer )
	{
		for(var i=0;i<this.runningTimers.length;i++)
		{
			var r=this.runningTimers[i];
			if (r.timer==timer)
			{
				r.checkpoints.length=0;
			}
		}
	}

	checkpoint( timer, event )
	{
		for(var i=0;i<this.runningTimers.length;i++)
		{
			var r=this.runningTimers[i];
			if (r.timer==timer)
			{
				r.checkpoints.push( { event:event, timestamp:new Date().getTime() } );
				return;
			}
		}
		var c=[];
		c.push( { event:event, timestamp:new Date().getTime() } );
		this.runningTimers.push( { timer:timer, checkpoints:c } );
	}

	show( timer )
	{
		var buffer=[];

		for(var i=0;i<this.runningTimers.length;i++)
		{
			var r=this.runningTimers[i];
			if (r.timer==timer)
			{
				if (r.checkpoints.length==0) return;
				var prev;
				for(var j=0;j<r.checkpoints.length;j++)
				{
					var c=r.checkpoints[j];
					buffer.push(
						this.getTemplate( 'timerCheckpointTpl' )
							.replace('%EVENT%',c.event)
							.replace('%TIMESTAMP%',c.timestamp)
							.replace('%DIFFERENCE%',(prev ? c.timestamp-prev : 0))
					);
					prev=c.timestamp;
				}

				var interval = r.checkpoints[r.checkpoints.length-1].timestamp - r.checkpoints[0].timestamp;
				if (this.prevService==this.service)
				{
					this.timerCumulative+=interval;
					this.executionCount++;
				}
				else
				{
					this.timerCumulative=interval;
					this.executionCount=1;
				}
			}
		}

		var avg = Math.round((this.timerCumulative/this.executionCount) * 100) / 100;
		$(this.selector_timer).html(
			this.getTemplate( 'timerListTpl' )
				.replace(/%ITEMS%/g,buffer.join("\n"))
				.replace(/%AVERAGE%/g,avg)
				.replace(/%EXEC_COUNT%/g,this.executionCount)
				.replace(/%SERVER%/g,this.server.label)
				.replace(/%QUERY%/g,this.lastQueryName.length>0 ? this.lastQueryName : "")
		);

		this.prevService=this.service;
	}

	setLastQueryName(lastQueryName)
	{
		this.lastQueryName = lastQueryName;
	}

	copyAverage()
	{
		functions.copyToClipboard($(this.selector_timer_clipboard).html().trim());
		this.setMessage('copied execution average to clipboard');
	}

	clear()
	{
		this.timerCumulative=0;
		this.executionCount=0;
		this.prevService=null;

		$(this.selector_timer).html(
			this.getTemplate( 'timerListTpl' )
				.replace(/%ITEMS%/g,"")
				.replace(/%AVERAGE%/g,0)
				.replace(/%EXEC_COUNT%/g,this.executionCount)
				.replace(/%SERVER%/g,"")
				.replace(/%QUERY%/g,"")
		);
	}

	toggle()
	{
		$('.timer').toggle(!this.timerVisible);
		this.timerVisible=!this.timerVisible;
		this.clear();
		this.setMessage('timer ' + (this.timerVisible ? 'on' : 'off') );
	}

}
