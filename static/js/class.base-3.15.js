class Base {

    config = null
    server = null
    service = null
    timer = null
    templates = [{ label: "messageCloseTpl", code: '&nbsp;<a href="#" onclick="$(\'#message\').fadeOut(100);return false;" class="no-underline">x</a>' },]
    template_groups = []
    message_selector = "#message"
    max_message_length = 150 // beware of cutting off hyperlinks

    VIEW_TYPE_JSON = "json"
    VIEW_TYPE_TABULAR = "tabular"

    constructor(config)
    {
        this.setConfig(config);
    }

    setConfig(config)
    {
        this.config = config;
    }

    setServer(server)
    {
        this.server = server;
    }

    setService(service)
    {
        this.service=service;
    }

    setTimer(server)
    {
        this.timer = timer;
    }

    setTemplates(templates)
    {
        this.templates = [].concat(this.templates, templates);
    }

    getTemplate(template)
    {
        var r=null;

        Array.from(this.templates).forEach(arg =>
        {
            if (arg.label==template)
            {
                r=arg.code;
            }
        })

        if (r==null)
        {
            console.log("missing template: " + this.constructor.name + "::" + template);
        }

        return r;
    }

    setTemplateGroup(template_group)
    {
        this.template_groups = [].concat(this.template_groups, template_group);
    }

    getTemplateGroup(template_group)
    {
        var r=null;
        Array.from(this.template_groups).forEach(arg =>
        {
            if (arg.label==template_group)
            {
                r=arg.group;
            }
        })
        return r;
    }

    setMessage( msg, fadeOut )
    {
        $(this.message_selector).stop(true,true);

        if (msg && msg.length>this.max_message_length)
        {
            msg = msg.substring(0,this.max_message_length) + "...";
        }

        if (!msg)
        {
            $(this.message_selector).fadeOut(100);
        }
        else
        if (fadeOut===false)
        {
            $(this.message_selector).toggle(true).html(msg);
            $(this.message_selector).append(this.getTemplate("messageCloseTpl"));
        }
        else
        {
            $(this.message_selector).toggle(true).html(msg).fadeOut(fadeOut ? fadeOut : 2000);
        }
    }

    getStoredItem( key, def )
    {
        return $.jStorage.get(key,def)
    }

}