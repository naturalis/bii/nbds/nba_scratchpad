class StoredQueries extends Base
{
    class_export = '.export'
    class_group = '.group'
    class_query = '.query'
    class_query_export = '.query-export'
    class_query_export_count = '.export-query-count'
    class_query_group_select = '.query-group-select'
    selector_monthly_backup_toggle = "#enableMonthlyBackup"
    selector_prefix_group = '#group-'
    selector_prefix_query = '#query-'
    selector_query = '#query'
    selector_query_list = '#queries'
    selector_query_name = '#queryName'
    selector_query_trigger = '#query-trigger'
    selector_ctrl_s_toggle = '#enableCtrlS'
    selector_notes = '#notes'
    selector_notes_name = '#notesName'
    selector_notes_div = '#notesDiv'
    selector_notes_handle = '#notesDragHandle'
    selector_notes_size = '#notesSize'
    selector_notes_link = '#dynamicNotesOutLink'
    selector_export_options = '#exportOptions'

    // TODO: also use these in regexp's
    storage_query_prefix = "query:"
    storage_notes_prefix = "notes:"

    queries = []
    services = []

    lastLoadedQueryName = null
    activeQuery = ""
    oldNotes = ""
    initialNote = true
    newQueryIndex = null
    isLoadingBackup = false;

    constructor(config,server)
    {
        super(config);
        this.setServer(server);
    }

    setQueries()
    {
        this.queries.splice(0,this.queries.length);
        this.queries = Array.from($.jStorage.index()).filter(function(arg)
        {
            return arg.indexOf(this.storage_query_prefix)==0;
        },this)
    }

    getQueries()
    {
        return this.queries;
    }

    setServices(services)
    {
        this.services = services;
    }

    setIsLoadingBackup(isLoadingBackup)
    {
        this.isLoadingBackup = isLoadingBackup;
    }

    getIsLoadingBackup()
    {
        return this.isLoadingBackup;
    }

    cleanUp()
    {
        // clean up empty keys
        $.jStorage.deleteKey("");

        // clean up orphaned notes
        var queries = [];
        var notes = [];

        Array.from($.jStorage.index()).forEach(a =>
        {
            if (a.indexOf(this.storage_query_prefix)==0)
            {
                queries.push(a.substring(this.storage_query_prefix.length));
            }
            else
            if (a.indexOf(this.storage_notes_prefix)==0)
            {
                notes.push(a.substring(this.storage_notes_prefix.length));
            }
        });

        notes.filter(value => !queries.includes(value)).forEach(function(note)
        {
            $.jStorage.deleteKey(this.storage_notes_prefix+note);
        },this);
    }

    setMonthlyBackupSetting()
    {
        $.jStorage.set("setting:auto-backup",$(this.selector_monthly_backup_toggle).prop('checked') ? "y" : "n");
    }

    getMonthlyBackupSetting()
    {
        return $.jStorage.get("setting:auto-backup")=="y";
    }

    setMonthlyBackupToggle()
    {
        $(this.selector_monthly_backup_toggle).prop('checked',this.getMonthlyBackupSetting()).trigger('change');
    }

    runAutoQuerybackup()
    {
        if (!this.getMonthlyBackupSetting())
        {
            return;
        }

        var run = true;
        var now = new Date();
        var last = $.jStorage.get("setting:last-backup");

        if (last!=null)
        {
            last = new Date(last);
            run = (((now.getTime()-last.getTime())/1000/86400/30)>1);
        }

        if (run)
        {
            this.quickExport( true, "nba-scratchpad-queries--auto-backup--" +
                this.server.label.replace(/http(s)?:\/\//,"").replace(/:(\d)+$/,"") + "--" +
                now.getFullYear()+"."+functions.leftPad(now.getMonth()+1,2)+"."+functions.leftPad(now.getDate(),2)
            );
            $.jStorage.set("setting:last-backup",now);
        }
    }

    quickExport( export_all=false, filename=null )
    {
        if (export_all)
        {
            var exp=[];
            Array.from($.jStorage.index()).forEach(arg=>
            {
                if (arg.indexOf("setting:")==-1)
                {
                    exp.push({key:arg,value:this.getStoredItem(arg)});
                }
            });
            var buffer=JSON.stringify(exp);
        }
        else
        {
            var prev=$(this.selector_query).val();
            this.exportQueryToggleAll(true);
            this.export();

            var date_string = functions.getDateString(true,true);
            var buffer=$(this.selector_query).val();
            $(this.selector_query).val(prev);
        }

        if (filename==null)
        {
            filename="nba-scratchpad-queries--"+date_string;
        }

        buffer =
            "//nba query scratchpad export ("+date_string+")\n" +
            "//to import, open scratchpad (http://api.biodiversitydata.nl/scratchpad/), access query maintenance by clicking 'more options', and choose the file to import.'.\n" +
            "//alternatively, you can copy/paste the contents of this file into the query window, and choose 'import queries from query window'.\n" +
            buffer;

        functions.createLocalDownloadFile(buffer, filename+".json");
    }

    selectedQueryCount()
    {
        var n=0;
        $(this.class_query_export).each(function()
        {
            if ($(this).prop('checked')==true)
            {
                n++;
            }
        });
        $(this.class_query_export_count).html(n);
    }

    exportQueryToggleAll( state )
    {
        $(this.class_query_export).each(function()
        {
            $(this).prop('checked',state);
            $(this).trigger('change');
        });

        $(this.class_query_group_select).each(function()
        {
            $(this).prop('checked',state);
            $(this).trigger('change');
        });
    }

    export()
    {
        var selected=[];
        var self = this;

        $(this.class_query_export).each(function()
        {
            if ($(this).prop('checked')==true)
            {
                var t = self.queries[parseInt($(this).attr('id').replace('export-',''))];
                selected.push(t);
                selected.push(t.replace(/^query:/,self.storage_notes_prefix));
            }
        },self);

        if (selected.length==0) return;

        var exp=[];

        Array.from($.jStorage.index()).forEach(arg =>
        {
            if (selected.indexOf(arg)!=-1)
            {
                exp.push({key:arg,value:this.getStoredItem(arg)});
            }
        });

        $(this.selector_query).val(JSON.stringify(exp));
    }

    createExampleQueries(force=false)
    {
        // if (!force && this.queries.length>0) return;

        Array.from(this.getTemplateGroup("example_queries")).forEach(arg=>
        {
            var labels = arg.data.filter(function(a) { return a.label=="label"; })
            var paths = arg.data.filter(function(a) { return a.label=="path"; })
            var label;
            var path;

            if (labels.length>0)
            {
                label = labels[0].value;
            }

            if (paths.length>0)
            {
                path = paths[0].value;
            }

            if (!label)
            {
                return;
            }

            if (path)
            {

                $.jStorage.set(this.storage_query_prefix + label,arg.tpl.trim()+this.config.databasefieldSeparator+path.trim().hashCode());
            }
            else
            {
                $.jStorage.set(this.storage_query_prefix + label,arg.tpl.trim());
            }
        });

        this.setMessage("added example queries");
        this.setQueries();
        this.printList();
    }

    printList()
    {
        $(this.selector_query_list).empty();

        Array.from(this.queries).forEach(function(query,i)
        {
            var q=query.replace(/^query:/,"");
            var matches = q.match(this.config.queryGroupRegExp);
            var t=this.getStoredItem(query);

            if (!t.length)
            {
                // deleting empty orphans
                $.jStorage.deleteKey(query);
                return;
            }

            var t=this.getStoredItem(query).split(this.config.databasefieldSeparator);
            var n=this.getStoredItem(this.storage_notes_prefix+q,"");

            if(t[1])
            {
                var s=null;

                Array.from(this.services).forEach(arg=>
                {
                    if (arg.key==t[1])
                    {
                        s=arg;
                    }
                });
            }
            else
            {
                var s=false;
            }

            if (matches)
            {
                var groupName=matches[0];
                var hashCode=groupName.hashCode();

                if (q.replace(groupName,"").trim().length==0)
                {
                    q=i;
                }

                if (!$('#group-'+hashCode).length)
                {
                    $(this.selector_query_list).append(this.getTemplate( 'groupTpl' ).replace(/%TITLE%/g,groupName.replace(/^\[/,"").replace(/\]$/,"")).replace(/%HASH%/g,hashCode));
                }

                $('#group-'+hashCode)
                    .append(this.getTemplate( 'queryTpl' )
                        .replace(/%NUMBER%/g,i)
                        .replace(/%TITLE%/g,q).replace(groupName,"").trim()
                        .replace('%SERVICE%',s ? " service '"+s.label+"'" : "out service")
                        .replace('%ARROW%',s ? this.getTemplate( 'useQuerySymbolAndServiceTpl' ) : this.getTemplate( 'useQuerySymbolTpl' ))
                        .replace('%NOTES%',n ? this.getTemplate( 'noteSymbolTpl' ) : this.getTemplate( 'emptyNoteSymbolTpl' ))
                        .replace('%NOTE_CHAR_COUNT%',n ? ' ('+n.length+')' : '' )
                );
            }
            else
            {
                $(this.selector_query_list)
                    .append(this.getTemplate( 'queryTpl' )
                        .replace(/%NUMBER%/g,i)
                        .replace(/%TITLE%/g,q)
                        .replace('%SERVICE%',s ? " service '"+s.label+"'" : "out service")
                        .replace('%ARROW%',s ? this.getTemplate( 'useQuerySymbolAndServiceTpl' ) : this.getTemplate( 'useQuerySymbolTpl' ))
                        .replace('%NOTES%',n ? this.getTemplate( 'noteSymbolTpl' ) : this.getTemplate( 'emptyNoteSymbolTpl' ))
                        .replace('%NOTE_CHAR_COUNT%',n ? ' ('+n.length+')' : '' )
                    );
            }
        },this);

        $(this.class_group).on('click',{ caller: this },function(e)
        {
            $(e.data.caller.selector_prefix_group+$(this).attr('data-id')).toggle();
        });

        $('ul[id^=group-]').each(function(index, element)
        {
            $('#'+$(this).attr('id').replace('group-','count-')).html($(this).children().length);
        });

    }

    saveQuery()
    {
        var index=$(this.selector_query_name).val().trim();
        if (index.length==0)
        {
            this.setMessage("need a name");
            return;
        }

        var query=$(this.selector_query).val().trim();
        if (query.length==0)
        {
            this.setMessage("need a query");
            return;
        }

        index=functions.htmlEncode(index);
        this.newQueryIndex="query:"+index;

        if (this.service.key)
        {
            query+=this.config.databasefieldSeparator+this.service.key;
        }

        $.jStorage.set(this.storage_query_prefix+index,query);
        $(this.selector_query_name).val("");
        this.setMessage("saved '" + index + "'");
        this.setQueries();
    }

    saveQueryViaKeyboard(e)
    {
        if ($(this.selector_ctrl_s_toggle).is(':checked'))
        {
            e.preventDefault();

            if ($('.queryHighlight').length>0)
            {
                var name = this.queries[$('.queryHighlight').first().attr("data-number")].replace(/^query:/,"");
                //var backup=getStoredItem('query:'+name).split(config.databasefieldSeparator);
                $(this.selector_query_name).val(name);
                this.saveQuery()
            }
        }
    }

    getQuery( number )
    {
        return this.getStoredItem(this.queries[number]).split(this.config.databasefieldSeparator);
    }

    getNote( query )
    {
        return this.getStoredItem(this.storage_notes_prefix+query.replace(/^query:/,""),"");
    }

    useQuery( number )
    {
        var t=this.getQuery(number);

        $(this.selector_query).val(t[0]);

        this.lastLoadedQueryName=this.queries[number].replace(/^query:/,"");

        return t[1] ? t[1] : null; // returning service key (a.k.a. hashCode)
    }

    highlightQuery( number )
    {
        $(this.class_query).removeClass('queryHighlight');
        $(this.selector_prefix_query+number).addClass('queryHighlight');
        $(this.selector_prefix_query+number).addClass('queryHighlight');
    }

    highlightNewQuery()
    {
        var num=null;
        Array.from(this.queries).forEach(function(arg,i)
        {
            if (this.newQueryIndex==arg)
            {
                num=i;
            }
        },this);

        this.highlightQuery(num);

        var matches = this.newQueryIndex.match(this.config.queryGroupRegExp);

        if (matches)
        {
            $('#group-'+matches[0].hashCode()).toggle(true);
        }
    }

    deleteQuery(number)
    {
        if (confirm("are you sure?"))
        {
            $.jStorage.deleteKey(this.queries[number]);
            $.jStorage.deleteKey(this.queries[number].replace(/^query:/,this.storage_notes_prefix));
            this.setMessage("deleted '" + this.queries[number].replace(/^query:/,"") + "'");
            this.setQueries();
            this.createExampleQueries();
            this.printList();
        }
    }

    useQueryName(number)
    {
        var name = functions.htmlDecode(this.queries[number].replace(/^query:/,""));
        $(this.selector_query_name).val(name);

        if (bindKeys.getShiftPressed())
        {
            functions.copyToClipboard(name);
        }
    }

    useGroupName(name)
    {
        $(this.selector_query_name).val('[' + name + '] ');

        if (bindKeys.getShiftPressed())
        {
            functions.copyToClipboard(name);
        }
    }

    autoPullQueryTrigger()
    {
        if (bindKeys.getCtrlPressed())
        {
            $(this.selector_query_trigger).trigger('click');
        }
    }

    hideExportOptionsWindow()
    {
        if ($(this.selector_export_options).is(':visible'))
        {
            $(this.class_export).toggle();
        }
    }

    switchNotes( number )
    {
        if ($(this.selector_notes_div).is(':visible'))
        {
            this.showNotes(number,true);
        }
    }

    showNotes( number, dontClose )
    {
        var query=this.queries[number].replace(/^query:/,"");
        if (this.activeQuery==query && !dontClose)
        {
            $(this.selector_notes_div).toggle();
        }
        else
        {
            var newNotes = $(this.selector_notes).val().trim();
            if (newNotes!==this.oldNotes && !this.initialNote)
            {
                this.saveNotes();
            }
            this.activeQuery=query;
            this.oldNotes = this.getStoredItem(this.storage_notes_prefix+this.activeQuery,"");

            $(this.selector_notes).val(this.oldNotes);
            $(this.selector_notes_name).html(this.activeQuery);
            $(this.selector_notes_div).toggle(true);

            this.setNoteSize();
            this.initialNote=false;

            $(this.selector_notes_div)
                .css({
                    'left': ( $(this.selector_prefix_query+number).position().left + 14 ) + 'px',
                    'top': (
                        $(this.selector_prefix_query+number).position().top +
                        $(this.selector_prefix_query+number).height() +2 ) + 'px'
                });

            $( this.selector_notes_div ).draggable({ handle: this.selector_notes_handle });
        }
    }

    saveNotes()
    {
        var notes = $(this.selector_notes).val().trim();
        var index = this.storage_notes_prefix+this.activeQuery;

        if (this.oldNotes.length==0 && notes.length==0)
        {
            return;
        }
        else
        if (notes.length==0)
        {
            $.jStorage.deleteKey(index);
            this.setMessage("deleted note");
        }
        else
        {
            $.jStorage.set(index,notes);
            this.setMessage("saved note");
        }

        this.setQueries();
        this.printList();
    }

    setNoteSize()
    {
        $(this.selector_notes_size).html("("+$(this.selector_notes).val().trim().length+")");
    }

    detectLinkInNotes()
    {
        var candidate=$(this.selector_notes).val().substring($(this.selector_notes).prop('selectionStart'),$(this.selector_notes).prop('selectionEnd'));
        var res = candidate.match(/((((http|https):(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/i);
        if (res)
        {
            $(this.selector_notes_link).attr('href',candidate).attr('title',candidate).toggle(true);
        }
        else
        {
            $(this.selector_notes_link).toggle(false);
        }
    }

    insertStringInNotes( str )
    {
        var cursorPos = $(this.selector_notes).prop('selectionStart');
        var val = $(this.selector_notes).val();
        var text="";

        if (str=="%date%")
        {
            var date = new Date();
            text = date.toDateString() + ", " + date.getHours() + "h" + date.getMinutes() + ": ";
        }
        else
        if (str=="%line%")
        {
            text = "-----------------------------------------\n";
        }
        else
        {
            text = str;
        }

        $(this.selector_notes).val(val.substring(0,cursorPos) + text + val.substring(cursorPos,val.length));
        this.setNoteSize();
    }

    notesClose()
    {
        $(this.selector_notes_div).toggle(false);
    }

    importFromQueryWindow()
    {
        this.queryImport($(this.selector_query).val());
    }

    queryImport( raw )
    {
        if (!raw)
        {
            raw=$(this.selector_query).val().trim();
        }

        raw = functions.removeCommentsFromText(raw,this.config.commentCharacters);

        if (raw.length==0)
        {
            return;
        }

        try
        {
            JSON.parse(raw);
        }
        catch (err)
        {
            functions.dialog( "query import error", this.getTemplate( "importErrorTpl" ).replace("%ERROR%",err) );
            return;
        }

        functions.dialog("query import options",this.getTemplate( "importOptionsTpl" ),main.doQueryImport,raw);
    }

    makeUniqueKey( key )
    {
        var keys=$.jStorage.index();
        var nkey=key;
        var n=1;
        while (keys.indexOf(nkey)!=-1) {
            nkey=key+" ("+(n++)+")";
        }
        return nkey;
    }

    doQueryImport( raw )
    {
        this.setQueries();

        // skip, overwrite, append
        var import_strategy = $("input[name='import_strategy']:checked").val();

        try {
            var imp=JSON.parse(raw);
            var saved=0;
            var updated=0;
            var skipped=0;
            for (var i=0;i<imp.length;i++)
            {
                var q=imp[i];
                // console.log(q.key);
                if (q.key && q.value)
                {
                    var index = this.queries.indexOf(q.key);
                    if (index==-1 || import_strategy=="append")
                    {
                        $.jStorage.set(this.makeUniqueKey(q.key),q.value);
                        saved++;
                    }
                    else
                    if (import_strategy=="overwrite")
                    {
                        $.jStorage.set(q.key,q.value);
                        updated++;
                    }
                    else
                    {
                        skipped++;
                    }
                }
            }
            this.setMessage( "added "+saved+" queries; updated " + updated + "; skipped " + skipped );
            $(this.selector_query).val("");
            $(this.class_export).toggle();
            this.setQueries();
            this.printList();
        }
        catch(ex)
        {
            this.setMessage( "error occurred: " + ex, 10000 );
        }
    }

    queryDeleteSelected()
    {
        if (confirm("are you sure you want to delete?"))
        {
            var selected=[];

            $(this.class_query_export).each(function()
            {
                if ($(this).prop('checked')==true)
                {
                    selected.push(parseInt($(this).attr('id').replace('export-','')));
                }
            });

            if (selected.length==0) return;

            var queries = this.queries.filter(function(a,i) { return selected.indexOf(i)>-1; });
            var notes = queries.map(x => x.replace(/^query:/,this.storage_notes_prefix));

            Array.from($.jStorage.index()).forEach(arg=>
            {
                if (queries.indexOf(arg)!=-1 || notes.indexOf(arg)!=-1)
                {
                    $.jStorage.deleteKey(arg);
                }
            });

            this.setMessage("queries deleted");
            $(this.class_export).toggle();
            this.setQueries();
            this.createExampleQueries();
            this.printList();
        }
    }
}