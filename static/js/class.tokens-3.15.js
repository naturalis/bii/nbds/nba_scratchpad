class Tokens extends Base
{
	selector_token_query_trigger = '#token-query-trigger'
	selector_result_query = '#resultQuery'

	token = null
	tokenServicePath = null
	batchCount = 1
	recordCount = 0
	batchTotalCount = null
	triggerBlinked = false
	requestObject = null
	data = null
	latestResultSize = null
	server = null

	setRequestObject(requestObject)
	{
		this.requestObject = requestObject;
	}

	getRequestObject()
	{
		return this.requestObject;
	}

	setData(data)
	{
		this.data = data;
	}

	setLatestResultSize(latestResultSize)
	{
		this.latestResultSize = latestResultSize;
	}

	setServer(server)
	{
		this.server = server;
	}

	getToken()
	{
		return this.token;
	}

	processToken()
	{
		this.storeToken();
		this.toggleTokenButton();
		this.updateBatchNumbers();
	}

	resetBatchNumbers()
	{
		this.batchCount=1;
		this.recordCount=0;
		this.batchTotalCount=null;
		// this.printBatchNumbers();
	}

	updateBatchNumbers()
	{
		// last batch has no token, hence check
		if (this.requestObject.servicePath.indexOf("/batchQuery")==-1)
		{
			return;
		}

		this.recordCount += this.latestResultSize;

		if (this.batchTotalCount==null)
		{
			var p={
				method: "POST",
				url: this.server.proxyUrl + this.server.proxyPath,
				data: {
					method: "GET",
					service: this.requestObject.servicePath.replace("/batchQuery","/count"),
					data: JSON.stringify(this.requestObject.payload)
				},
				success: function(data, status, xhr)
				{
					tokens.batchTotalCount = data;
					tokens.printBatchNumbers();
				}
			};

			$.ajax( p );
		}
		else
		{
			this.printBatchNumbers();
		}
	}

	printBatchNumbers()
	{
		$('.batch-count-and-total').html(this.getTemplate('retrievedBatchCountTpl')
			.replace('%TOTAL%',Intl.NumberFormat().format(this.batchTotalCount))
			.replace('%BATCHES%',Intl.NumberFormat().format(this.batchCount))
			.replace('%RETRIEVED%',Intl.NumberFormat().format(this.recordCount))
		);
	}

	storeToken()
	{
		if (this.data.token)
		{
			this.token = this.data.token;
		}
		else
		{
			this.token = undefined;
		}
	}

	toggleTokenButton()
	{
		if (this.triggerBlinked)
		{
			$(this.selector_token_query_trigger).removeClass('do-blink');
		}

		$('.token-query-group').toggle(this.token!=undefined);

		if (!this.triggerBlinked && this.token!=undefined)
		{
			$(this.selector_token_query_trigger).addClass('do-blink');
			this.triggerBlinked=true;
		}
	}

	increaseBatchCount()
	{
		this.batchCount++;
	}
}