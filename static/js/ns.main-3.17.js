var main = {

	onLoad()
	{
		jQuery.fn.center = function () {
		  this.css("position","absolute");
		  this.css("top", Math.max(0, (($(window).height() - $(this).outerHeight()) / 2) + $(window).scrollTop()) + "px");
		  this.css("left", Math.max(0, (($(window).width() - $(this).outerWidth()) / 2) + $(window).scrollLeft()) + "px");
		  return this;
		}

		String.prototype.hashCode = function() {
		  var hash = 0, i, chr, len;
		  if (this.length === 0) return hash;
		  for (i = 0, len = this.length; i < len; i++) {
		    chr   = this.charCodeAt(i);
		    hash  = ((hash << 5) - hash) + chr;
		    hash |= 0; // Convert to 32bit integer
		  }
		  return hash;
		};

		$.fn.selectRange = function(start, end)
		{
		    if(end === undefined)
		    {
		        end = start;
		    }
		    return this.each(function()
		    {
		        if('selectionStart' in this)
		        {
		            this.selectionStart = start;
		            this.selectionEnd = end;
		        } else
		        if(this.setSelectionRange)
		        {
		            this.setSelectionRange(start, end);
		        } else
		        if(this.createTextRange)
		        {
		            var range = this.createTextRange();
		            range.collapse(true);
		            range.moveEnd('character', end);
		            range.moveStart('character', start);
		            range.select();
		        }
		    });
		};
	},
	servicesOnChange()
	{
		services.setService();
		query_window.setService(services.getService());
		services_gui.setService(services.getService());
		query.setService(services.getService());
		request.setService(services.getService());
		stored_queries.setService(services.getService());
		query_window.setQueryHint();
		gui.setService(services.getService());
		gui.setResultWindowUrl();
		gui.toggleAddExplain();
		timer.setService(services.getService());
	},
	useQuery(number)
	{
		var key = stored_queries.useQuery(number);
		if (key)
		{
			services_gui.selectServiceByKey(key);
		}
		stored_queries.highlightQuery(number);
		stored_queries.autoPullQueryTrigger();
		stored_queries.switchNotes(number);
		timer.setLastQueryName(stored_queries.lastLoadedQueryName);
	},
	executeQuery()
	{
		query_window.setText();
		query_window.tidyText();
		query_window.setText();
		query_window.replaceEmptyText();

		var q = query_window.getText();
		q = functions.removeCommentsFromText(q,scratchpad.config.commentCharacters);

		query.setQuery(q)
		query.parsePathParams();
		query.verifyValidForm();

		if (!services.getService().noQuery && !query.getIsValidQuery())
		{
			var err = query.getError();
			err = gui.makeClickableJsonParseError(err);
			query.setMessage(err,false);
			return;
		}

		query_history.storeQuery(q,services.getService().key,services.getService().label);
		query_history.capStack();
		post_processor.clear(true);

		query.setMessage();
		json_path.clearPathResults();
		gui.clearResultSearch();

		request.setCustomUrl();
		request.setService(services.getService());
		request.setQuery(query.getQuery());
		request.setPathParams(query.getPathParams());
		request.setQueryEncoded(query.getEncodedQuery());
		request.setRequestObject();

		gui.setResultWindowUrl(request.getRequestObject().fullRequestUrl);

		tokens.resetBatchNumbers();
		results.clear();
		results.setLatestResult({});
		gui.setWaitCursor(true);
		request.doRequest();
		gui.setWaitCursor(false);
	},
	executeAlteredQuery()
	{
		request.setCustomUrl($('#resultQuery').val().trim());
		request.setRequestObject();
		results.clear();
		results.setLatestResult({});
		gui.setWaitCursor(true);
		request.doRequest();
		gui.setWaitCursor(false);
	},
	processRequestResult(r)
	{
		request.timer.checkpoint( request.timer.QUERY_EXECUTION, 'got succesful response' );
		request.setResponseStatus(r.status);

		results.setLog();
		results.setLatestResult(r.data);
		results.resetDisplayData();
		results.applyResultDisplayCap()
		results.setFormatted();
		results.setFullRequestUrl(request.getRequestObject().fullRequestUrl);
		results.setContentType(results.getLatestResult().contentType);
		results.setServer(scratchpad.getServer());
		results.printResults();

		request.timer.checkpoint( request.timer.QUERY_EXECUTION, 'finished processing response' );

		tabular_results.resetData();
		tabular_results.setResultData(results.getLatestResult());
		tabular_results.bootstrapWindow();
		tabular_results.flattenResultSet();
		tabular_results.makeTsvData();
		tabular_results.toggleResultViewIcons();

		request.timer.checkpoint( request.timer.QUERY_EXECUTION, 'made tabular' );

		results.setLatestResultProperty({tsv:tabular_results.getTsvData()});

		output_hashes.store(results.getStringified());
		output_hashes.print();

		request.timer.checkpoint( request.timer.QUERY_EXECUTION, 'processed hashes' );

		tokens.setRequestObject(request.getRequestObject());
		tokens.setData(results.getLatestResult().data);
		tokens.setLatestResultSize(results.getLatestResultSize());
		tokens.setServer(scratchpad.getServer());
		tokens.processToken();

		request.timer.checkpoint( request.timer.QUERY_EXECUTION, 'processed token' );
		json_path.setLatestResults(results.getLatestResult().data);
		request.timer.show( request.timer.QUERY_EXECUTION );
		post_processor.process();
	},
	processRequestError(r)
	{
		// processing of errors that originate in the proxy; nba-errors are processed in results.printResults()
		request.timer.checkpoint( request.timer.QUERY_EXECUTION, "got error response" );
		request.timer.show( request.timer.QUERY_EXECUTION );
		scratchpad.setMessage( "request error: " + r.statusText + " (" +r.status +")", false );
		request.setResponseStatus(r.status);
		results.resetLatestResult();
		tabular_results.bootstrapWindow();
		results.setRequestError(r.status,r.statusText,request.getRequestObject().fullRequestUrl);
		results.showRequestError();
	},
	executeTokenQuery()
	{
		var token = tokens.getToken();

		if (token)
		{

			var obj = tokens.getRequestObject();

			obj.fullRequestUrl = (obj.fullRequestUrl.replace(/\?_(.*)$/,"")+"?_token=").replace(/\?+/,"?") + token;
			obj.method = "GET";
			obj.payload = { "_token" : token }

			request.setCompleteRequestObject(obj);

			$("#resultQuery").val(request.getRequestObject().fullRequestUrl);

			results.clear();
			request.doRequest();
			tokens.increaseBatchCount();
		}
	},
	useTabularHeadersAsFields()
	{
		var h = tabular_results.getTabularHeaders();
		query_window.insertQueryElement("custom",{"property": "fields", "code": JSON.stringify(h)});
	},
    readUploadFile( e )
    {
        var file = e.target.files[0];
        if (!file)
        {
            // console.log("no file");
            return;
        }

        var reader = new FileReader();
        reader.onload = function(e)
        {
            var contents = e.target.result;
            stored_queries.queryImport( contents );
            $('.export').toggle();
            $('#file-input').val("");
            stored_queries.setIsLoadingBackup(false);
        };
        reader.readAsText(file);
    },
    doQueryImport(content)
    {
    	stored_queries.doQueryImport(content);
    },
    useSessionItem(id)
    {
        var this_query = query_history.getItem(id);
        if (this_query)
        {
        	services_gui.selectServiceByKey(this_query.endpoint_key);
            query_window.setQueryWindowValue(this_query.query);
            stored_queries.autoPullQueryTrigger();
        }
    },
    switchToJSONResultView()
    {
		var json = tabular_results.getTemplate( 'responseSuccessTpl' )
			.replace('%RESPONSE%',tabular_results.result_data.formatted);
		$( tabular_results.selector_local_results ).html(json);
		results.siteCoordinatesBindOnClick();
		results.docKeysBindOnClick();
    }

}
