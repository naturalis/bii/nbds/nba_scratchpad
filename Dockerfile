FROM ubuntu:18.04

RUN apt-get update
RUN apt-get -y install python3-pip
RUN pip3 install --upgrade pip

COPY . /app
WORKDIR /app

RUN pip3 install -r requirements.txt

ENTRYPOINT ["python3"]
CMD ["scratchpad.py"]
