#!/bin/bash

export FLASK_PORT=8088
export FORCE_GET_REQUESTS=false
export PUBLIC_NBA_ADDRESS=api.biodiversitydata.nl
export PRIVATE_NBA_ADDRESS=api.biodiversitydata.nl

#export PUBLIC_NBA_ADDRESS=api.nba-a.bii.dryrun.link
#export PRIVATE_NBA_ADDRESS=api.nba-a.bii.dryrun.link


python3 scratchpad.py 
