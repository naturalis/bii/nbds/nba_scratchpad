#!/bin/bash

TAG=$1
COMMIT=$2


GIT_BRANCH=$(git branch --show-current)

if [[ $GIT_BRANCH != "master" ]]; then
    echo "tagging is meant for releases on the master branch (currently on: $GIT_BRANCH); exiting."
    exit
fi

if [[ -z $TAG ]]; then
    echo -n "enter tag: "
    read TAG
fi

if [[ -z $TAG ]]; then
    echo "need a tag; exiting"
    exit
fi

ARRAY=($TAG)

if [[ ! -z ${ARRAY[1]} ]]; then
    echo "tag can't contain spaces"
    exit
fi

if [[ -z $COMMIT ]]; then
    echo -n "enter commit message (empty for none, '=' for copy of tag): "
    read COMMIT
fi

if [[ $COMMIT == "=" ]]; then
    COMMIT=$TAG
fi

echo
echo "tagging with: "

if [[ ! -z $COMMIT ]]; then
    COMMIT=$(echo $COMMIT | sed -e "s/'//")
    echo "git tag  -a $TAG -m '$COMMIT'"
else
    echo "-a $TAG"
fi

echo
echo "press a key to continue"
read


git tag -a $TAG -m "$COMMIT"
git tag

echo
echo "press a key to push tags"
read

git push --follow-tags

